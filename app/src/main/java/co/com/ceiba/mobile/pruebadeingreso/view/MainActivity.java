package co.com.ceiba.mobile.pruebadeingreso.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Layout;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.master.permissionhelper.PermissionHelper;

import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapter.AdapterEmpty;
import co.com.ceiba.mobile.pruebadeingreso.adapter.AdapterUsuarios;
import co.com.ceiba.mobile.pruebadeingreso.baseDatos.BaseDatosLocal;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Post;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Usuario;
import co.com.ceiba.mobile.pruebadeingreso.service.MyApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity implements Callback<ArrayList<Usuario>>, AdapterUsuarios.VerPost {

    private ArrayList<Usuario> listaUsuarios;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaUsuarios = new ArrayList<>();
        cargarPermisos();
        if(BaseDatosLocal.crearBaseLocal()){
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Cargando datos");
            progressDialog.show();
            iniciarServicio();
        }

    }

    private void cargarPermisos() {

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            PermissionHelper permissionHelper = new PermissionHelper(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 100);
            permissionHelper.request(new PermissionHelper.PermissionCallback() {
                @Override
                public void onPermissionGranted() {
                    Log.d("TAG", "onPermissionGranted() called");
                }

                @Override
                public void onIndividualPermissionGranted(String[] grantedPermission) {
                    Log.d("TAG", "onIndividualPermissionGranted() called with: grantedPermission = [" + TextUtils.join(",",grantedPermission) + "]");
                }

                @Override
                public void onPermissionDenied() {
                    Log.d("TAG", "onPermissionDenied() called");
                }

                @Override
                public void onPermissionDeniedBySystem() {
                    Log.d("TAG", "onPermissionDeniedBySystem() called");
                }
            });
        }



    }

    @Override
    protected void onStart() {
        super.onStart();

        EditText editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                listaUsuarios.clear();
                listaUsuarios = BaseDatosLocal.obtenerUsuarios(s.toString().trim());
                init();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void init(){

        RecyclerView listaUusariosRecycle = findViewById(R.id.recyclerViewSearchResults);
        listaUusariosRecycle.setHasFixedSize(true);

        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext());
        listaUusariosRecycle.setLayoutManager(layout);

        if(listaUsuarios.size()==0){

            AdapterEmpty adapterUsuarios = new AdapterEmpty();
            listaUusariosRecycle.setAdapter(adapterUsuarios);
        }else {

            AdapterUsuarios adapterUsuarios = new AdapterUsuarios(listaUsuarios, this);
            listaUusariosRecycle.setAdapter(adapterUsuarios);
        }


        if(progressDialog!=null)
          progressDialog.dismiss();
    }

    @Override
    public void onResponse(Call<ArrayList<Usuario>> call, Response<ArrayList<Usuario>> response) {
        if(response.isSuccessful()){
            listaUsuarios = response.body();
            BaseDatosLocal.registrarUsuarios(listaUsuarios);
            Log.d("cantidad datos", "Tamano "+listaUsuarios.size());
            init();
        }
    }

    @Override
    public void onFailure(Call<ArrayList<Usuario>> call, Throwable t) {

    }

    @Override
    public void OnClickVerPost(int position) {

        Intent intent = new Intent(this, PostActivity.class);
        intent.putExtra( "USUARIO" , listaUsuarios.get(position));
        startActivity(intent);
    }

    public void iniciarServicio() {



        Call<ArrayList<Usuario>> call =  MyApiService.getApiService().getUsuarios();
        call.enqueue(this);

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(BaseDatosLocal.crearBaseLocal()){

            iniciarServicio();
        }
    }
}