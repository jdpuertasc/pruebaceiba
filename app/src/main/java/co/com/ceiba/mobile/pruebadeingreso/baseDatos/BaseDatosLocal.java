package co.com.ceiba.mobile.pruebadeingreso.baseDatos;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.objetos.Usuario;

public class BaseDatosLocal {

    public static String mensaje;

    public static boolean crearBaseLocal() {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(DirApp(), "datos.db");
            db = SQLiteDatabase.openOrCreateDatabase(dbFile, null);

            String usuario = "CREATE TABLE IF NOT EXISTS USUARIOS(id varchar(20), name varchar(50), username  varchar(50), email varchar(50))";
            db.execSQL(usuario);
            return true;

        } catch (Exception e) {

            mensaje = e.getMessage();
            return false;

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static void registrarUsuarios(ArrayList<Usuario> lista ) {

        SQLiteDatabase db = null;

        try {

            File dbFile = new File(DirApp(), "datos.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);
                db.delete("USUARIOS", null, null);

                for(Usuario usuario: lista) {

                    ContentValues values = new ContentValues();
                    values.put("id", usuario.getId());
                    values.put("name", usuario.getName());
                    values.put("username", usuario.getUsername());
                    values.put("email", usuario.getEmail());

                    db.insertOrThrow("USUARIOS", null, values);
                }

            } else {

                Log.i("Error", "GUARDAR USUARIOS: No Existe la Base de Datos Config.db o No tiene Acceso a la SD");

            }

        } catch (Exception e) {

            mensaje = e.getMessage();

        } finally {

            if (db != null)
                db.close();
        }
    }

    public static ArrayList<Usuario> obtenerUsuarios(String nombreUsuario) {

        Usuario usuario = null;
        SQLiteDatabase db = null;
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();

        try {

            File dbFile = new File(DirApp(), "datos.db");

            if (dbFile.exists()) {

                db = SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READWRITE);

                String query = "SELECT id, name, username, email  FROM USUARIOS WHERE name LIKE '%"+nombreUsuario+"%'";
                Cursor cursor = db.rawQuery(query, null);

                if (cursor.moveToFirst()) {

                    do {
                        usuario = new Usuario();
                        usuario.setId(cursor.getString(cursor.getColumnIndex("id")));
                        usuario.setName(cursor.getString(cursor.getColumnIndex("name")));
                        usuario.setUsername(cursor.getString(cursor.getColumnIndex("username")));
                        usuario.setEmail(cursor.getString(cursor.getColumnIndex("email")));

                        listaUsuarios.add(usuario);
                    }while (cursor.moveToNext());
                }

                if (cursor != null)
                    cursor.close();

            }

        } catch (Exception e) {

            mensaje = e.getMessage();
            Log.e("usuarios", mensaje, e);

        } finally {

            if (db != null)
                db.close();
        }

        return listaUsuarios;
    }

    public static File DirApp() {
        File SDCardRoot = Environment.getExternalStorageDirectory(); // Environment.getDataDirectory(); //
        File dirApp = new File(SDCardRoot.getPath() + "/PRUEBACEIBA"  );
        // File dirApp = new File(SDCardRoot + "/data/" + "co.com.SuperRicas/" +
        // Const.nameDirApp);
        if (!dirApp.isDirectory()) dirApp.mkdirs();
        return dirApp;
    }

}
