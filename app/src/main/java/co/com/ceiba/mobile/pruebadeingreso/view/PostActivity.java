package co.com.ceiba.mobile.pruebadeingreso.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.adapter.AdapterPosts;
import co.com.ceiba.mobile.pruebadeingreso.adapter.AdapterUsuarios;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Post;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Usuario;
import co.com.ceiba.mobile.pruebadeingreso.service.MyApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends Activity implements Callback<ArrayList<Post>> {

    private Usuario usuario;
    private TextView name, phone, email;
    private ArrayList<Post> listPost;
    private RecyclerView listaPostRecycle;
    private AdapterPosts adapterPosts;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Cargando datos");
        progressDialog.show();
        initComponentes();
    }

    private void initComponentes() {

        name =(TextView) findViewById(R.id.name);
        phone =(TextView) findViewById(R.id.phone);
        email =(TextView) findViewById(R.id.email);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("USUARIO")) {
                usuario = (Usuario) bundle.get("USUARIO");
            }
        }

        listPost= new ArrayList<>();

        Call<ArrayList<Post>> call =  MyApiService.getApiService().getPost(usuario.getId());
        call.enqueue(this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        name.setText(usuario.getName());
        phone.setText(usuario.getPhone());
        email.setText(usuario.getEmail());

    }


    @Override
    public void onResponse(Call<ArrayList<Post>> call, Response<ArrayList<Post>> response) {
        if(response.isSuccessful()){
            listPost = response.body();
            Log.d("cantidad datos", "Tamano "+listPost.size());
            init();
        }
    }


    @Override
    public void onFailure(Call<ArrayList<Post>> call, Throwable t) {

    }

    public void init(){
        listaPostRecycle = findViewById(R.id.recyclerViewPostsResults);
        listaPostRecycle.setHasFixedSize(true);

        LinearLayoutManager layout = new LinearLayoutManager(getApplicationContext());
        listaPostRecycle.setLayoutManager(layout);

        adapterPosts = new AdapterPosts(listPost);
        listaPostRecycle.setAdapter(adapterPosts);

        if(progressDialog!=null)
            progressDialog.dismiss();

    }


}
