package co.com.ceiba.mobile.pruebadeingreso.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Post;


public class AdapterPosts extends RecyclerView.Adapter<AdapterPosts.ViewHolder> {

    private ArrayList<Post> listaPost;
    private View view;

    public AdapterPosts(ArrayList<Post> listaUsuarios) {
        this.listaPost = listaUsuarios;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_list_item, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Post post = listaPost.get(position);

        holder.title.setText(post.getTitle());
        holder.body.setText(post.getBody());


    }

    @Override
    public int getItemCount() {
        return listaPost.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {

         private TextView title, body;

        public ViewHolder(View view){
            super(view);
            title = view.findViewById(R.id.title);
            body = view.findViewById(R.id.body);

        }

    }



}
