package co.com.ceiba.mobile.pruebadeingreso.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.R;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Usuario;


public class AdapterUsuarios extends RecyclerView.Adapter<AdapterUsuarios.ViewHolder> {

    private ArrayList<Usuario> listaUsuarios;
    private VerPost post;

    public AdapterUsuarios(ArrayList<Usuario> listaUsuarios, VerPost post ) {
        this.listaUsuarios = listaUsuarios;
        this.post = post;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item, parent, false);

        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Usuario usuario = listaUsuarios.get(position);
        if(usuario!=null) {
            holder.usuario.setText(usuario.getName());
            holder.telefono.setText(usuario.getPhone());
            holder.email.setText(usuario.getEmail());
        }

    }

    @Override
    public int getItemCount() {

        return listaUsuarios.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

         private TextView usuario, telefono, email;
         private Button btnVerPublicacion;

        public ViewHolder(View view){
            super(view);
            usuario = view.findViewById(R.id.name);
            telefono = view.findViewById(R.id.phone);
            email = view.findViewById(R.id.email);
            btnVerPublicacion = view.findViewById(R.id.btn_view_post);

              btnVerPublicacion.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            post.OnClickVerPost(getAdapterPosition());
        }
    }

    public interface VerPost {
        void OnClickVerPost(int position);
    }


}
