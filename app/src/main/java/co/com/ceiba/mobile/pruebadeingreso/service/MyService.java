package co.com.ceiba.mobile.pruebadeingreso.service;


import java.util.ArrayList;

import co.com.ceiba.mobile.pruebadeingreso.objetos.Post;
import co.com.ceiba.mobile.pruebadeingreso.objetos.Usuario;
import co.com.ceiba.mobile.pruebadeingreso.rest.Endpoints;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MyService {

    @GET(Endpoints.GET_USERS)
    Call<ArrayList<Usuario>> getUsuarios();

    @GET(Endpoints.GET_POST_USER)
    Call<ArrayList<Post>> getPost(
            @Query("userId") String userId
    );

}
